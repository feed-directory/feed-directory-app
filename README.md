## Feed-Directory app

Feed-Directory aims to be a feed (atom, rss,...) indexer.

In its v0.1 this app will allow:

* submit an opml
* submit a feed or a website url (feed detection)
* list / search known feeds with basic info

### Run this app

Here is my workflow but it might not work at the moment on your side:

* Database and messaging:

```
# init a postgres database with default values
podman run --rm -p 5432:5432 -e POSTGRES_USER=fd -e POSTGRES_PASSWORD=fd -e POSTGRES_DB=feeddirectory postgres
# init nats.io to manage subscriptions
podman run --rm -p 4222:4222 -p 8222:8222 -ti nats:latest
```

* init python environment:

```
python -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

* start the app:

```
cd src
# init database schema
python ./test.py init_db
```

```
cd src
# repeat this operation to get as much workers you want
# it will start a worker and subscribe to feed.new message
python ./test.py listen
```

```
cd src
# you need an opml file not provided in this repo
python ./test.py opml
```