from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from feed_directory.config import Config

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
#FIXME: flask migrate dosn't work because model is not loaded
migrate = Migrate(app, db)

# import other librairies after initialization
from feed_directory import model