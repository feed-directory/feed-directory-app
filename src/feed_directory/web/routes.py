from flask import render_template, request
from feed_directory import app
from feed_directory.web.controllers import FeedController,OPMLController
import json
from urllib.parse import urlparse

#TODO: move this elsewhere
def is_url(url):
  try:
    result = urlparse(url)
    return all([result.scheme, result.netloc])
  except ValueError:
    return False


@app.route('/')
def index():
    return render_template('home.html', feed_number=FeedController.get_known_feeds())
@app.route('/submit/opml',methods = ['GET', 'POST'])
def submit_opml():
    feeds = None
    submission_result = None
    if request.method == "POST":
        if 'opml_file' in request.files:
            f = request.files['opml_file']
            feeds = OPMLController.get_opml_feed_url(f.read())
        urls = []
        for i in range(1,int(len(request.form) / 2) + 1):
            if request.form.get(f"submit_{i}") == 'on':
                urls.append(request.form.get(f"url_{i}"))
        if len(urls) > 0:
            print(f"Sending {len(urls)} feeds")
            submission_result = FeedController.send_new_feeds(urls)
            
    return render_template('submit_opml.html', feeds_from_opml = feeds, feed_number=FeedController.get_known_feeds(), submission_result=json.loads(submission_result) if submission_result else None)
@app.route('/submit/url',methods = ['GET', 'POST'])
def submit_url():
    submission_result = None
    if request.method == "POST":
        url = request.form.get('url')
        if is_url(url):
            submission_result = FeedController.send_new_feeds([url])
        else:
            submission_result = f"[ \"Invalid url : {url}\"]"
        

    return render_template('submit_url.html', feed_number=FeedController.get_known_feeds(), submission_result=json.loads(submission_result) if submission_result else None)