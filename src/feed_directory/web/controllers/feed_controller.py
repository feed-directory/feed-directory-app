import requests
import json
from feed_directory import Config
class FeedController:
    @staticmethod
    def get_known_feeds():
        r = requests.get(f"{Config.BACKEND_URL}/api/feeds/count")
        if r.status_code == 200:
            return int(json.loads(r.text)['feed_count'])
        else:
            return None
    
    @staticmethod
    def send_new_feeds(urls):
        r = requests.post(f"{Config.BACKEND_URL}/api/feeds/submit", json = json.dumps(urls))
        if r.status_code == 200:
            return r.text
        else: 
            return "error"