import requests
import json
from feed_directory import Config

class OPMLController:

    @staticmethod
    def get_opml_feed_url(opml_content):
        r = requests.post(f"{Config.BACKEND_URL}/api/opml/submit",
        data=opml_content)
        if r.status_code == 200:
            return json.loads(r.text)
        else:
            return None
    

        
