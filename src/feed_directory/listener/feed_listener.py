import asyncio
from nats.aio.client import Client as NATS
from nats.aio.errors import ErrConnectionClosed, ErrTimeout, ErrNoServers
from feed_directory.controller import FeedController
from feed_directory import db, Config
import concurrent.futures

class FeedListener:
    nc: NATS = None

    def __init__(self):
        self.nc = NATS()

    async def feed_handler(self,msg):
        asyncio.create_task(self.feed_task(msg))
    
    async def feed_task(self,msg):
        result = await  self.loop.run_in_executor(self.executor, self.new_feed, msg) 
        print(f"sending result: {result}")
        await self.nc.publish(msg.reply, result.encode())

    def new_feed(self,msg):
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        if not FeedController.is_feed_knwon(data):
            f = FeedController.get_feed(data)
            if f:
                if not f.id: 
                    print(f"saving {f.url}")
                    db.session.add(f)
                    db.session.commit()
                reply_msg = f"{data}: new feed -> OK"
            else:
                reply_msg = f"{data}: new feed -> KO"
        else:
            reply_msg = f"{data}: existing feed"
            print(f"{data}: existing feed")
        #FIXME: send a structured reply message
        return reply_msg

    async def run(self,loop):
        self.loop = loop
        # Executor for tasks in parallel.
        self.executor = concurrent.futures.ThreadPoolExecutor(
         max_workers=5,
        )

        await self.nc.connect(Config.NATS_URI, loop=self.loop)
        print("subscribing to feed.new")
        await self.nc.subscribe("feed.new", "feed", cb=self.feed_handler)
        
    async def stop(self):    
        await self.nc.drain()