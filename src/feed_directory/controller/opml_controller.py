from os import stat
import requests, atoma, hashlib,json
from nats.aio.client import Client as NATS
import asyncio
from feed_directory.model import Feed
from .feed_controller import FeedController
from feed_directory import Config
from nats.aio.errors import ErrConnectionClosed, ErrTimeout
class OPMLController:

    @staticmethod
    async def request_handler(msg):
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        print(f"submitted: {data}")

    @staticmethod
    async def send_new_feeds(loop,feeds_url):
        nc = NATS()
        await nc.connect(servers=[Config.NATS_URI], loop=loop)
        for f in feeds_url:
            print(f"sending {f}")
            try:
                response = await nc.request("feed.new", f.encode(), cb=OPMLController.request_handler, timeout=5)
            except ErrTimeout:
                print(f"no response for {f}")

        await nc.flush()


    @staticmethod
    def get_feed(outline):

        feeds_url = []
        if outline.outlines and len(outline.outlines) > 0:
            for o in outline.outlines:
                for f in OPMLController.get_feed(o):
                    if f and f.startswith("http"):
                        if f not in feeds_url:
                            feeds_url.append(f)
        else:
            if outline.xml_url and outline.type:
                if outline.xml_url.startswith("http") and outline.xml_url not in feeds_url:
                    feeds_url.append(outline.xml_url)
        
        return feeds_url
    
    @staticmethod
    def submit_feed(feeds_url):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(OPMLController.send_new_feeds(loop, feeds_url))


    @staticmethod
    def parse_opml(opml_file_path):
        opml = atoma.parse_opml_file(opml_file_path)
        feeds_url = []
        for outline in opml.outlines:
            for f in OPMLController.get_feed(outline):
                feeds_url.append(f)

        return feeds_url

    @staticmethod
    def parse_opml_data(opml_file_data):
        opml = atoma.parse_opml_bytes(opml_file_data)
        feeds_url = []
        for outline in opml.outlines:
            for f in OPMLController.get_feed(outline):
                feeds_url.append(f)

        return feeds_url