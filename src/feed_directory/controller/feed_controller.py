import requests
import feedparser
from feed_directory.model import Feed, FeedGeneratedInfo
from feed_directory.utils import TextUtil
import hashlib
from datetime import datetime
from feed_directory import Config
from nats.aio.client import Client as NATS
from nats.aio.errors import ErrConnectionClosed, ErrTimeout
import asyncio
class FeedController:
    @staticmethod
    def is_feed_knwon(feed_url):
        feed = None
        feed_hash = hashlib.sha256(feed_url.encode()).hexdigest()
        feed = Feed.query.filter_by(hash=feed_hash).first()
        return True if feed else False
    @staticmethod
    def get_feed_number():
        return Feed.query.count()

    @staticmethod
    def get_feed(feed_url):
        headers = {
            'User-Agent': 'Feed Directory 1.0'
        }
        feed_response = None
        feed = None
        feed_hash = hashlib.sha256(feed_url.encode()).hexdigest()
        feed = Feed.query.filter_by(hash=feed_hash).first()
        if feed is None:
            feed = Feed()
            feed.hash = feed_hash
            feed.url = feed_url
        else:
            print(f"{feed_url} already known")
            return feed
        try:
            feed_response = requests.get(feed_url, headers=headers, timeout=2)
        except:
            print(f"Error while getting {feed_url}")
            return None
        if feed_response and feed_response.status_code == 200:
            feed_generated_info = FeedGeneratedInfo()
            try:
                data = feedparser.parse(feed_response.content)
                feed.title = data.feed.title
                feed.type = data.version
                feed.generator = data.feed.generator if 'generator' in data.feed else 'unkown'
                feed.description = data.feed.subtitle if 'subtitle' in data.feed else ''
                tags = []
                if 'tags' in data.feed:
                    for t in data.feed.tags:
                        tags.append(t.term)
                feed.tags = ','.join(tags[0:10])

                feed.site_url = data.feed.link if 'link' in data.feed else ''
                feed.logo_url = data.feed.image.href if 'image' in data.feed and 'href' in data.feed.image else ''
                if 'update_parsed' in data.feed:
                    feed.generated = datetime(*data.feed.updated_parsed[0:6])
                elif 'published_parsed' in data.feed:
                    feed.generated = datetime(*data.feed.published_parsed[0:6])
                elif len(data.entries) > 0 and 'published_parsed' in data.entries[0]:
                    feed.generated = datetime(*data.entries[0].published_parsed[0:6])
                else:
                    feed.generated = None
                feed.updated = datetime.now()
                feed.rights = data.feed.rights if 'rights' in data.feed else ''
                feed_generated_info.num_of_entries = len(data.entries)

            except Exception as e:
                print(f"Error while parsing {feed_url}")
                print(e)
                print('---')
                print(data.feed)
                print('---')
                return None
        else:
            print(f"Error while getting {feed_url}")
            return None
        #if sample_of_entries != "":
        #    feed_generated_info.lang = TextUtil.get_text_language(sample_of_entries)
        #if len(word_counts) > 0:
        #    feed_generated_info.word_count_avg = round(sum(word_counts) / len(word_counts),0)
        #else:
        #    print("cannot detect language no content")
        #if len(categories) > 0:
        #    feed_generated_info.categories = ','.join(categories)
        
        #feed.generated_info.append(feed_generated_info)

        return feed

    @staticmethod
    async def send_a_feed(nc, url):
        try:
            result = await nc.request("feed.new", url.encode(),timeout=5)
            print(f"--> {result.data.decode()}")
            return result.data.decode()
        except ErrTimeout:
            print(f"no worker respond in 5s for {url}")
        except ErrConnectionClosed:
            print(f"no queue available for {url}")

    @staticmethod
    async def send_new_feeds(loop,feeds_url):
        nc = NATS()
        await nc.connect(servers=[Config.NATS_URI], loop=loop)
        statements = []
        for f in feeds_url:
            statements.append(FeedController.send_a_feed(nc,f))
        results = await asyncio.gather(*statements)
        await nc.drain()
        return results

    @staticmethod
    def submit_feed(feeds_url):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        results = loop.run_until_complete(FeedController.send_new_feeds(loop, feeds_url))
        loop.close()
        print(results)
        return results