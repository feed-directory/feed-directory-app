from html.parser import HTMLParser
from polyglot.text import Detector

class HTMLToText(HTMLParser):
    text = ''
    def handle_data(self,data):
        self.text += data

class TextUtil:

    @staticmethod
    def get_text_language(html_data):
        try:
            detector = Detector(text=TextUtil.get_text_from_html(html_data))
            if detector.reliable:
                return detector.language.code
            else:
                print("text detector: language not reliable")
                return None
        except Exception as e:
            print(f"error while detecting language: {e}")
            return None
    @staticmethod
    def get_text_from_html(html_data):
        h2t = HTMLToText()
        h2t.feed(html_data)
        return h2t.text
    
    @staticmethod
    def get_word_count(html_data):
        text = TextUtil.get_text_from_html(html_data)
        return len(text.split(' '))
        
