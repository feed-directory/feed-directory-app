from flask import request
from feed_directory import app
from feed_directory.controller import FeedController,OPMLController
import json

@app.route('/api/feeds/count')
def feeds_count():
    return { "feed_count": FeedController.get_feed_number()}

@app.route('/api/feeds/submit', methods = ['POST'])
def submit_url():
    urls = json.loads(request.json)
    result = FeedController.submit_feed(urls)
    if result:
        return json.dumps(result),200
    else:
        return "error",500
@app.route('/api/opml/submit', methods = ['POST'])
def opml_submit():
    opml_content = request.data
    feeds_url = OPMLController.parse_opml_data(opml_content)
    return json.dumps(feeds_url)


