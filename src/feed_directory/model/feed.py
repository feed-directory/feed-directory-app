from flask_sqlalchemy import SQLAlchemy
from feed_directory import db
from datetime import datetime

class Feed(db.Model):
    """
    Feed represents a Feed with basic information it provides
    """
    __tablename__ = "feeds"
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(10))
    generator = db.Column(db.String(100))
    title = db.Column(db.String(500))
    description = db.Column(db.String(1000))
    tags = db.Column(db.String(500))
    rights = db.Column(db.String(500))
    url = db.Column(db.String(300))
    hash = db.Column(db.String(64))
    site_url = db.Column(db.String(300))
    logo_url = db.Column(db.String(300))
    generated = db.Column(db.DateTime)
    updated = db.Column(db.DateTime)
    created = db.Column(db.DateTime, default = datetime.now)
    generated_info = db.relationship('FeedGeneratedInfo', backref='feed', lazy='dynamic')

class FeedGeneratedInfo(db.Model):
    """
    FeedGeneratedInfo represents some computed information at a specific date
    """
    __tablename__ = "feed_generated_info"
    id = db.Column(db.Integer, primary_key=True)
    feed_id = db.Column(db.Integer, db.ForeignKey('feeds.id'))
    #feed = db.relationship('Feed')
    date = db.Column(db.DateTime, default = datetime.now)
    num_of_entries = db.Column(db.Integer)
    categories = db.Column(db.String(500))
    word_count_avg = db.Column(db.Integer)
    lang = db.Column(db.String(5))