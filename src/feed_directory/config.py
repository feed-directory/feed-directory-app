import os

class Config:

    def get_env(name, default=''):
        return os.environ[name] if name in os.environ else default

    DB_TYPE= get_env('DB_TYPE', 'postgres')
    SQLITE_FILE = get_env('SQLITE_URI','db.sqlite3')
    NATS_URI = get_env('NATS_URI','nats://127.0.0.1:4222')
    POSTGRES_URL = get_env('POSTGRES_URL','127.0.0.1:5432')
    POSTGRES_USER = get_env('POSTGRES_USER','fd')
    POSTGRES_PW = get_env('POSTGRES_PW','fd')
    POSTGRES_DB = get_env('POSTGRES_DB','feeddirectory')
    BACKEND_URL = get_env('BACKEND_URL','http://localhost:5001')

    if DB_TYPE == 'sqlite':
        SQLALCHEMY_DATABASE_URI = f"sqlite:///{SQLITE_FILE}"
    elif DB_TYPE == 'postgres':
        SQLALCHEMY_DATABASE_URI = f"postgresql+psycopg2://{POSTGRES_USER}:{POSTGRES_PW}@{POSTGRES_URL}/{POSTGRES_DB}"
    SQLALCHEMY_TRACK_MODIFICATIONS = False