from feed_directory import db
import feed_directory.model
from feed_directory.utils import HTMLToText, TextUtil
from feed_directory.controller import OPMLController,FeedController
from feed_directory import app
import feed_directory.web
from polyglot.downloader import downloader
from polyglot.text import Text
import asyncio
from feed_directory.listener import FeedListener
import sys
from nats.aio.client import Client as NATS
from nats.aio.errors import ErrConnectionClosed, ErrTimeout

def polyglot_test():
    #print(downloader.supported_languages_table("pos2"))
    #downloader.download("pos2.fr")
    #downloader.download("embeddings2.fr")
    #downloader.download("TASK:embeddings2", lang='fr', quiet=False)
    with open('test.txt','r') as f:
        data = f.read()
        text = Text(TextUtil.get_text_from_html(data))
        print(TextUtil.get_text_language(data))
        word_presence = {}
        for tag in text.pos_tags:
            if (tag[1] == "PROPN" or tag[1] == "NOUN") and len(tag[0]) > 2:
                if tag[0] in word_presence:
                    word_presence[tag[0]] += 1
                else:
                    word_presence[tag[0]] = 1
        print(list(sorted(word_presence.items(), key=lambda item: -item[1]))[0:5])
        print(text)
def recreate_db():
    db.drop_all()
    db.create_all()

def test_cat():
    f = FeedController.get_feed('http://googlecode.blogspot.com/atom.xml')
    print(f.generated_info[0].word_count_avg)



def opml_test():
    feeds = OPMLController.parse_opml('/home/vincent/work/tmp/feed_directory/tt-rss_vincent_2021-03-01.opml')
    #feeds = OPMLController.parse_opml('/home/vincent/work/perso/feed-directory/sample/feedly_seb.opml')
    return feeds

def nats_listen():
    print("Starting feed listener")
    fl = FeedListener()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(fl.run(loop))
    loop.run_forever()
    #loop.close()
        
async def send_message(loop):
    nc = NATS()
    await nc.connect(servers=["nats://127.0.0.1:4222"], loop=loop)
    await nc.publish("feed.new", b'https://blog.valvin.fr/index.xml')
    await nc.flush()

if __name__ == "__main__":
    if len(sys.argv) == 2 :
        if sys.argv[1] == "listen":
            nats_listen()
        elif sys.argv[1] == "opml":
            opml_test()
        elif sys.argv[1] == "get_feed":
            test_cat()
        elif sys.argv[1] == "init_db":
            recreate_db()
        elif sys.argv[1] == "web":
            app.run()
            
    else:
        print("sending a message")
        loop = asyncio.get_event_loop()
        loop.run_until_complete(send_message(loop))

    #recreate_db()
    #feeds = opml_test()
    #for f in feeds:
    #    db.session.add(f)
    #    db.session.commit()
    #test_cat()
