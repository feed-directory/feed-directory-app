import sys


if __name__ == "__main__":
    if len(sys.argv) == 2 :
        if sys.argv[1] == "ui":
            from feed_directory import app
            import feed_directory.web
            app.run(port=5000)
        if sys.argv[1] == "api":
            from feed_directory import app
            import feed_directory.api
            app.run(port=5001)
        elif sys.argv[1] == "listen":
            from feed_directory.listener import FeedListener
            import asyncio
            fl = FeedListener()
            loop = asyncio.get_event_loop()
            loop.run_until_complete(fl.run(loop))
            loop.run_forever()